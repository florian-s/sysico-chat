let express = require('express');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let port = process.env.PORT || 3000;
let db = require('./db');
let bodyParser = require("body-parser");
let session = require("express-session");
let nunjucks = require("nunjucks");

//------------ AUTH ---------------

let passport = require('passport')
let LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  //must return done(err) ou done(null, user) ou done(null, false) si l'utilisateur n'existe pas ou mot de passe faut
  function(username, password, done) {
    //return done(null, {'id':0, 'username':"tartanpion", 'password':"password"});
    db.users.findByUsername(username, function(err, user) {
      if (err) { console.log(err); return done(err); }
      if (!user) {
        console.log("incorrect username");
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!db.users.validatePassword(user, password)) {
        console.log("incorrect password");
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

//way the user is serialized to be kept in session, we only keep the id
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  db.users.findById(id, function(err, user) {
    done(err, user);
  });
});
// ----------- EXPRESS --------------

let sessionMiddleware = session({ secret: "efesf:kjbe;fjhqv s145",
                                  resave: false,
                                  saveUninitialized: false
                                });
let passportSessionMiddleware = passport.session();

app.use(express.static(__dirname + '/public'));
app.use(sessionMiddleware);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passportSessionMiddleware);

//template engine
nunjucks.configure('views', {
    express: app,
    autoescape: true
});
app.set('view engine', 'html');


app.get('/', function(req, res){
  res.render("index.html");
});

app.get('/login', function(req, res){
  res.render("login.html");
});
app.post('/login',
  passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/login?failed=true'})
);
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/login');
});

app.get('/register', function(req, res){
  res.render("register.html");
});
app.post('/register', function(req, res) {
    if(req.body.password != req.body.password2)
      return res.render("register.html", {error: "Passwords don't match"});

    db.users.createUser(req.body.username, req.body.password, function(err, id) {
        console.log("Register "+req.body.username+" password "+req.body.password);
        if(err)
          return res.render("register.html", {error: err});
        else
          return res.redirect("/login");
    });
      
});


//use session middleware to have retrieve session information and
//make them accessible in the socket.request object
io.use(function(socket, next){
        // Wrap the express middleware
        sessionMiddleware(socket.request, {}, next);
})
/* middleware to check authentication and deserialize user to have acces to it in socket.request.user */
.use(function(socket, next){
  console.log(socket.request.session);
  
  if(!socket.request.session.passport || !socket.request.session.passport.user) {
    let err  = new Error('Authentication error');
    err.data = { type : 'not_authenticated' };
    return next(err);
  }

  passport.deserializeUser(socket.request.session.passport.user, function(err, user) {
    if(err) {
      return next(err);
    }
    socket.request.user = user;
    return next();
  });

});
io.on('connection', function(socket){
  console.log("A user connected");

  let infoMsgData = { type: "connection", msg: "L'utilisateur "+socket.request.user.username+" s'est connecté" };
  io.emit('info', infoMsgData);

  socket.on('chat message', function(msg){
    let msgData = { user: socket.request.user, msg: msg };
    io.emit('chat message', msgData);
  });

  socket.on('disconnect', function(reason) {
    let infoMsgData = { type: "disconnection", msg: "L'utilisateur "+socket.request.user.username+" s'est déconnecté" };
    io.emit('info', infoMsgData);
  });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
