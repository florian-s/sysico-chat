var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('db.sqlite');
var bcrypt = require("bcrypt");

let saltRounds = 10;
let defaultImageUrl = "default.png";

db.serialize(function() {
  db.run(`CREATE TABLE if not exists users(
                                      id INTEGER UNIQUE,
                                      username TEXT UNIQUE,
                                      password TEXT,
                                      image_url TEXT,
                                      PRIMARY KEY(id)
                                  )`);
});

let users = {}

users.findById = function(id, cb) {
  process.nextTick(function() {
    let stmt = db.prepare("SELECT id, username, image_url FROM users WHERE id=?");
    stmt.get([id], function(err, row) {
      if(err) {
        cb(err);
      } else if (typeof(row) === "undefined") {
        cb(null, false);
      } else {
        cb(null, row);
      }
    }); //get
  }); //nextTick
}

users.findByUsername = function(username, cb) {
  process.nextTick(function() {
    let stmt = db.prepare("SELECT id, username, password FROM users WHERE username=?");
    stmt.get([username], function(err, row) {
      if(err) {
        cb(err);
      } else if (typeof(row) === "undefined") {
        cb(null, false);
      } else {
        cb(null, row);
      }
    }); //get
  }); //nextTick
}

users.validatePassword = function(user, password) {
    return bcrypt.compare(password, user.password)
}

/*
 * Add user to the datase.
 * Parameters :
 * username, password (Strings)
 * cb : callback function : function(err, id)
 *      if an error occured, err is the error and id is null
 *      otherwise id is the id of the user in the databse
 */
users.createUser = function(username, password, cb) {
  let stmt = db.prepare("INSERT INTO users VALUES(NULL, ?, ?, ?)");

  bcrypt.hash(password, saltRounds, function(err, hash) {
    // Store hash in your password DB.
    if(err) {
      console.log(err);
      return cb(err, null);
    }

    stmt.run([username, hash, defaultImageUrl], function(err) {
      if(err) {
        console.log(err);
        return cb(err, null);
      }

      return cb(null, this.lastID);

    });
  });

}

exports.users = users;
