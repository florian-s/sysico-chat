$(function () {
    var socket = io();


    $('form').submit(function(){
        socket.emit('chat message', $('#m').val());
        $('#m').val('');
        return false;
    });

    /* Events */

    function createMessageElement(msgData) {
        let li = document.createElement('li');
        let span = document.createElement('span');
        span.innerText = msgData.user.username;
        li.appendChild(span);
        li.appendChild(document.createTextNode(msgData.msg));
        return li;
    }

    function createInfoElement(msgData) {
        let li = document.createElement('li');
        li.classList.add("info");
        li.classList.add(msgData.type);
        li.appendChild(document.createTextNode(msgData.msg));
        return li;
    }

    socket.on('chat message', function(msgData){
        let msgElement = createMessageElement(msgData);
        document.getElementById("messages").appendChild(msgElement);
        window.scrollTo(0, document.body.scrollHeight);
    });
    socket.on('info', function(msgData){
        let element = createInfoElement(msgData);
        document.getElementById("messages").appendChild(element);
        window.scrollTo(0, document.body.scrollHeight);
    });

    socket.on('error', function(err) {
        console.log(err);
        if(err.type == "not_authenticated") {
            window.location = "/login";
        }
    });
});
